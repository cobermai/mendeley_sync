import yaml

from flask import Flask, redirect, render_template, request, session
from mendeley import Mendeley
from mendeley.session import MendeleySession

from citeproc import CitationStylesStyle, CitationStylesBibliography
from citeproc import Citation, CitationItem
from citeproc import formatter
from citeproc.source.json import CiteProcJSON

import os

os.chdir(os.path.dirname(__file__))
conf = open('config.yml', 'r+')
config = yaml.load(conf, Loader=yaml.Loader)
REDIRECT_URI = "http://127.0.0.1:5000/oauth"


mendeley = Mendeley(config['clientId'], config['clientSecret'], REDIRECT_URI)

## interactive OAuth flow
# from https://python.hotexamples.com/examples/mendeley/Mendeley/-/python-mendeley-class-examples.html
if 'token' not in config:
  auth = mendeley.start_authorization_code_flow()
  state = auth.state
  auth = mendeley.start_authorization_code_flow(state=state)
  print(auth.get_login_url())

  #auth.authenticate('http://127.0.0.1:5000/oauth?code=xzP-AvzpsFdL1Yg4gDvKHUp-Sc0&state=F70XP3WZOWNI06PELD9QN1KKPPUPYQ')
  # After logging in, the user will be redirected to a URL, auth_response.
  session = auth.authenticate( input("Enter your full authorization link: ") )

  print(session.token)
  config['token'] = session.token

  ## clean file
  conf.write('')
  yaml.dump(config, conf, default_flow_style=False)
  print('New infos stored')

## update access tokens

## use new access token
session = MendeleySession(mendeley, config["token"])


def get_documents_by_group_id(group_id="63f2ce33-5dd2-33dc-9dd5-477a0dda1b2f"):
    results = []
    documents = session.group_documents(group_id).iter(view="all")
    for document in documents:
        new_dict = {
            'id': document.id,
            'title': document.title,
            'author': [{'family': autor.last_name, 'given': autor.first_name} for autor in document.authors],
            'type': document.type,
            'issued': {'date-parts': [[document.year if not document.year==None else False]]}
        }
        results.append(new_dict)
    return results

ref = get_documents_by_group_id(group_id='63f2ce33-5dd2-33dc-9dd5-477a0dda1b2f')
bib_style = CitationStylesStyle('harvard1', validate=False)

# from https://github.com/brechtm/citeproc-py
bib_source = CiteProcJSON(ref)
bibliography = CitationStylesBibliography(bib_style, bib_source, formatter.html)

for key in bib_source.keys():
    citation = Citation([CitationItem(key)])
    bibliography.register(citation)

file = open("sample.html", "w")
for i, item in enumerate(bibliography.bibliography()):
    file.write("\n [{}] ".format(i))
    file.write(str(item))
    print(str(item))
file.close()


